#!/usr/bin/env python3
import os
import logging
import re
import argparse
import requests
from time import sleep
from shutil import copyfile
from datetime import datetime, timedelta
from wikimarkup.parser import Parser  # pip install py-wikimarkup

parser = argparse.ArgumentParser(
    description="Parse a moin page dump to be imported into a DP Calendar on a Joomla site.\n"
                "Requires at least a standard subscription to the component."
                "Syntax:"
                "python moin2joomla.py -s /filepath/of/moin/code/data -t https://mysite.org -k 123456-42-123456 -o /images -i /home/user/tmp/images -c 5 -n Software Freedom Day -d debug")

parser.add_argument("-s", dest="src_path", default=r"/moindump",
                    help="Location of the path containing the data folder")
parser.add_argument("-t", dest="target_domain", default=r"https://freesoftwaretalk.org",
                    help="Domain of the website")
parser.add_argument("-k", dest="key", default=r"0000000",
                    help="API key for the Web Services client")
parser.add_argument("-o", dest="online_path", default="/images",
                    help="Base path where images will be found online")
parser.add_argument("-c", dest="calid", default="1",
                    help="ID of the calendar the events need to be added to")
parser.add_argument("-d", dest="detail", default="error",
                    choices=['warning', 'error', 'info', 'debug'],
                    help="Options: warning, error, info, debug")
parser.add_argument("-n", dest="event_name", default="Software Freedom Day",
                    help="Full name of the event to parse pages for - defaults to 'Software Freedom Day'")
parser.add_argument("-i", dest="image_folder", default="./images",
                    help="Temporary path to image folder (to be uploaded to root of /images folder of website")

args = parser.parse_args()

# Define the mapping between 'detail' argument values and logging levels
detail_to_logging_level = {
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'info': logging.INFO,
    'debug': logging.DEBUG
}

# Configure logging based on the 'detail' argument
logging_level = detail_to_logging_level.get(args.detail, logging.DEBUG)
logging.basicConfig(level=logging_level, format='%(asctime)s - %(levelname)s - %(message)s')

src_path = args.src_path
pages_path = os.path.join(src_path, "pages")
users_path = os.path.join(src_path, "user")
online_path = args.online_path
local_image_path = args.image_folder
domain = args.target_domain
token = args.key
calid = args.calid


def hex_to_utf8(match):
    hex_string = match.group(1)
    try:
        # Convert the entire hex string to bytes and then decode as UTF-8
        utf8_string = bytes.fromhex(hex_string).decode('utf-8')
    except Exception as e:
        # If decoding fails, log the error and return the original hex string
        utf8_string = f"({hex_string})"
        logging.error(f"Error decoding hex string {hex_string}: {e}")

    return utf8_string


def replace_hex_codes(s):
    # Regular expression pattern to find hexadecimal codes within parentheses
    pattern = re.compile(r'\(([0-9a-fA-F]+)\)')

    # Substitute found hex codes with their UTF-8 characters
    result_string = pattern.sub(hex_to_utf8, s)
    return result_string


def markup2html(content):
    markupParser = Parser()
    return markupParser.parse(content, show_toc=False)


def convert_wiki_links_to_html(html_content, file, filename, base_url=""):
    # Function to remove unwanted list elements from HTML
    def remove_unwanted_lists(match):
        list_content = match.group(0)
        # Check if the list elements are at the beginning of the HTML
        if html_content.startswith(list_content):
            return ''  # Remove the entire list
        return list_content

    # Regular expression pattern to find wiki links
    wiki_link_pattern = re.compile(r'\[\[([^]|]+)(?:\|([^]]+))?]]')

    # Function to convert wiki link match objects to HTML links
    def replace_wiki_link(match):
        page_name = re.sub('<[^<]+?>', '', match.group(1))
        try:
            link_text = re.sub('<[^<]+?>', '', match.group(2)) or page_name
        except TypeError as e:
            link_text = page_name
            logging.debug(f"Type Error on {link_text} - {type(e)}")
        except Exception as e:
            logging.error(f">> FAIL: {type(e)}")
            link_text = page_name
        href = base_url + page_name.replace(' ', '_') + '/index.html'
        return f'<a href="{href}">{link_text}</a>'

    # Regular expression pattern to find wiki attachments (images usually)
    wiki_attachments_pattern = re.compile(r'\{\{attachment: ?([^}]+)}}')

    # Function to convert wiki attachment to HTML image links
    def replace_wiki_attachments(match, file, filename):
        image_parts = match.group(1).split("|", 2)
        image_name = image_parts[0]
        image_alt = image_parts[1] if len(
            image_parts) > 1 else image_name.split(".")[0]
        image_style = "style='" + \
                      image_parts[2] + ";'" if len(image_parts) > 2 else ""

        # Attachments can have a comment field and markup too, divided by pipe symbols |
        # First block (always present) is filename, second is alt text, third and onward are CSS
        # Example: {{attachment:myLogo.png|My Organisation|width=100%,align="center"}}

        # copy image from attachments to new folder location
        try:
            from_path = os.path.join(pages_path, file.decode("utf-8"), "attachments")
        except AttributeError as e:
            logging.warning(f">> Encoding issue {e} on {file} - trying undecoded")
            from_path = os.path.join(pages_path, file, "attachments")
        to_path = os.path.join(local_image_path, filename)
        href_path = online_path + filename + image_name
        if not os.path.isfile(os.path.join(from_path, image_name)):
            try:
                os.makedirs(to_path, exist_ok=True)
                copyfile(os.path.join(from_path, image_name), os.path.join(to_path, image_name))
            except Exception as e:
                logging.warning(f">> SKIPPED FILE {os.path.join(from_path, image_name)} - reason: {type(e)}")

        return f'<img src="{href_path}" alt="{image_alt}" {image_style}/>'

    # Replace wiki links with HTML links
    html_with_links = wiki_link_pattern.sub(replace_wiki_link, html_content)
    # html_with_images = wiki_attachments_pattern.sub(replace_wiki_attachments, html_with_links)
    html_with_images = wiki_attachments_pattern.sub(
        lambda match: replace_wiki_attachments(match, file, filename), html_with_links
    )

    # Remove unwanted lists from the HTML
    html_without_lists = re.sub(
        r'<ol>.*?</ol>', remove_unwanted_lists, html_with_images, flags=re.DOTALL)

    return html_without_lists


def convert_epoch_to_date(epoch_millis):
    # Convert the epoch time in milliseconds to a human-readable date
    return (
        datetime.fromtimestamp(int(epoch_millis) / 1000000.0).strftime('%Y-%m-%d %H:%M:%S'))


def extract_edit_log_info(edit_log_path):
    try:
        with open(edit_log_path, 'r') as file:
            lines = file.readlines()

            if not lines:
                return None, None, None, None

            first_line = lines[0].split('\t')
            last_line = lines[-1].split('\t')

            # Convert timestamps to readable dates
            created_date = convert_epoch_to_date(first_line[0])
            created_user_id = first_line[6].strip()
            modified_date = convert_epoch_to_date(last_line[0])

            # Extract user ID from the last line
            modified_user_id = last_line[6].strip()

            return created_date, modified_date, created_user_id, modified_user_id

    except FileNotFoundError:
        logging.warning(f"Edit-log file not found: {edit_log_path}")
    except Exception as e:
        logging.error(f"Error reading edit-log at {edit_log_path}: {e}")

    return None, None, None, None


def get_author_details(user_id, user_data_directory):
    if user_id is None:
        user_id = ""
    user_file_path = os.path.join(user_data_directory, user_id)
    author_details = {'name': 'Unknown', 'email': 'Unknown'}

    try:
        with open(user_file_path, 'r') as user_file:
            # This part depends on how the user data is formatted
            # Let's assume it's a simple text file with 'Name: XXX' and 'Email: XXX'
            for line in user_file:
                if line.startswith('name='):
                    author_details['name'] = line.split('=', 1)[1].strip()
                elif line.startswith('email='):
                    author_details['email'] = line.split('=', 1)[1].strip()

    except FileNotFoundError:
        logging.warning(f"User file not found for user ID {user_id}")
    except Exception as e:
        logging.error(f"Error while reading user file for user ID {user_id}: {e}")

    return author_details

def is_within_bounding_box(lat, lon, bbox):
    south, north, west, east = map(float, bbox)
    return south <= lat <= north and west <= lon <= east

def populate_locations():
    locations = {}
    params = {
        "page[limit]": 5000,
        "filter['state']": 1
              }
    request = make_request(method="GET", endpoint="/dpcalendar/locations", params=params)
    if request:
        for item in request['data']:
            locations[item['id']] = item['attributes']

    logging.info(f"Generated locations file with {len(locations)} items.")
    return locations


def lookup_streetmap(organization=None, city=None, country=None):
    sleep(3)  # Always sleep 1 second prior to request to avoid accidental abuse of system

    response = requests.get(url="https://nominatim.openstreetmap.org/search",
                       headers={'User-Agent': 'Digital Freedom Foundation lookup script for migrating old data'},
                       params={'q': f"{organization if organization else ''} {city if city else ''} {country}",
                               'format': 'json', 'addressdetails': 1})

    if response.status_code != 200:
        logging.error(f">> ERROR {response.status_code} ({response.reason})")
        return False

    ## At this point we can presume HTTP Status code 200 - request succeeded

    if len(response.json()) == 0:
        # No result was found at this point
        # let's do a new request, but now without checking for the organization

        sleep(3)  # let's not upset OSM, they are our friends.
        response = requests.get(url="https://nominatim.openstreetmap.org/search",
                                headers={
                                    'User-Agent': 'Digital Freedom Foundation lookup script for migrating old data'},
                                params={'q': f"{city if city else ''} {country}",
                                        'format': 'json', 'addressdetails': 1})

        if response.status_code != 200:
            logging.error(f">> ERROR {response.status_code} ({response.reason})")
            return False

        if len(response.json()) == 0:
            logging.error(f">> ERROR; could not find {city if city else ''} {country if country else ''}")
            return False

        ## At this point we have reliably found a true location
        geolocation = response.json()[0]
        logging.info(f">> found {geolocation['name']}")
        return geolocation


def getlocation_id(organization=None, city=None, country=None):
    # This isn't optimal - refreshing the list on every request.
    # Good enough for now.
    locations = populate_locations()

    # geolocate on OpenStreetmap
    geolocation = lookup_streetmap(organization=organization, city=city, country=country)
    # let's try again, just based on city name
    if not geolocation:
        geolocation = lookup_streetmap(city=city, country=country)
    # Nope, we couldn't find it. Too bad.
    if not geolocation:
        return False

    # Search for all locations with applicable location parameters
    for location in locations:
        lat = float(locations[location]['latitude'])
        lon = float(locations[location]['longitude'])
        if is_within_bounding_box(lat, lon, geolocation['boundingbox']):
            logging.info(f"Looks like I found (1){organization} - (2){city} - (3){country}.")
            # Return the ID of the found location
            return locations[location]['id']

    # If we're at this point, it means we didn't find the location
    # If no location was found, create the newfound location

    title = f"Archived location for {organization if organization else ''} {city} {country}"
    description = f"{geolocation['display_name']}<br>Generated with the help op OpenStreetMap<br>{geolocation['licence']}"

    candidate_locations = make_request(method="GET", endpoint="/dpcalendar/locations", params= {
        "filter['search']": title,
        "page[limit]": 5000
    })

    if candidate_locations:
        try:
            pages = int(candidate_locations['meta']['total-pages'])
        except Exception as e:
            pages = 1
            print(f"{e}")

        for page in range(pages):
            logging.info(f"Loading locations page {page}/{pages}")

            if page > 0 :
                candidate_locations = make_request(method="GET", endpoint="/dpcalendar/locations", params={
                    "filter['search']": title,
                    "page[offset]": page+1,
                    "page[limit]": 5000
                })

            for candidate in candidate_locations['data']:
                # Still iterating through the results just to prevent "near hits"
                attributes = candidate['attributes']
                if attributes['title'] == title:
                    # The exact location exists, no need to duplicate
                    return candidate['id']

    # When getting here, no dupes were found, and we can generate.

    location_data = dict(title= title,
                         description=description,
                         country=country if country else geolocation['address']['country'],
                         city=city if city else "Unknown",
                         zip=geolocation['address']['postcode'] if 'postcode' in geolocation['address'] else "",
                         street=geolocation['address']['road'] if 'road' in geolocation['address'] else "N/A",
                         number=geolocation['address']['house_number'] if 'house_number' in geolocation[
                             'address'] else 'N/A', latitude=geolocation['lat'], longitude=geolocation['lon'])

    request = make_request(method="POST", endpoint="/dpcalendar/locations", data=location_data)
    if request:
        # Return the ID of the newly generated location
        return request['data']['id']


def get_next(eventname, year, format='%Y-%m-%d'):
    try:
        year = int(year)
    except ValueError as e:
        year = ""
    if eventname.lower() in ["document freedom day", "dfd"]:
        ## Document Freedom Day = Last Wednesday of March (month 3)
        ## Counting down from the last day to find a Wednesday
        date = datetime(year, 3, 31)
        while date.weekday() != 2:
            date -= timedelta(days=1)
        return date.strftime(format)

    elif eventname.lower() in ["hardware freedom day", "hfd"]:
        ## Hardware Freedom Day = 3rd Saturday of April
        ## The earliest possible day would be April 15th and counting up
        date = datetime(year, 4, 15)
        while date.weekday() != 5:
            date += timedelta(days=1)
        return date.strftime(format)

    elif eventname.lower() in ["software freedom day", "sfd"]:
        ## Software Freedom Day = 3rd Saturday of September
        ## The earliest possible day would be September 15th and counting up
        date = datetime(year, 9, 15)
        while date.weekday() != 5:
            date += timedelta(days=1)
        return date.strftime(format)
    else:
        ## Just pick the frist day of the next year
        date = datetime(year + 1, 1, 1)
        return date.strftime(format)


def get_current(filepath, file, filename):
    metadata = {}  # Start off with empty metadata (will gradually populate)
    current_path = os.path.join(filepath, "current")
    if not os.path.isfile(current_path):
        logging.warning(f">> SKIPPED {filepath}. Reason: No 'current' file.")
        return False  # , metadata

    try:
        with open(current_path) as current:
            raw_content = current.read()
            logging.debug(f"Raw Content: {repr(raw_content)}")
            # Check if unwanted content is at the beginning of the HTML
            if raw_content.startswith('<ol><li>'):
                raw_content = re.sub(
                    r'<ol>.*?</ol>', '', raw_content, flags=re.DOTALL)
            revision = raw_content.strip()
            prev_revision = revision[:-1] + str(int(revision[-1]) - 1)

    except FileNotFoundError as e:
        logging.warning(f">> File not found: current version for {current_path}. Reason: {e}")
        return False, metadata
    except IOError as e:
        logging.warning(f"Error reading file {current_path}: {e}")
        return False, metadata
    except Exception as e:
        logging.warning(f">> General error - Reason: {e}")
        return False, metadata

    edit_log_path = os.path.join(filepath, "edit-log")
    created_date, last_modified_date, created_user_id, modified_user_id = extract_edit_log_info(edit_log_path)
    created_author_details = get_author_details(created_user_id, users_path)
    modified_author_details = get_author_details(modified_user_id, users_path)

    if args.event_name.lower() in ('software freedom day', 'sfd'):
        event = "sfd"
    elif args.event_name.lower() in ('hardware freedom day', 'hfd'):
        event = "hfd"
    elif args.event_name.lower() in ('document freedom day', 'dfd'):
        event = "dfd"

    matches = re.findall(r"(?:^|/)([^/]+)", filename)
    if len(matches) > 4:
        # The last one is the continent, legacy
        # Year (0)0, country (1), city (2) and organization (3) are all available
        title = f"{args.event_name} {matches[0]} {matches[4]} ({matches[3]}, {matches[2]})"
        location_id = getlocation_id(organization=matches[4], city=matches[3], country=matches[2])
    elif len(matches) > 3:
        # It could be a legacy item if the continent is also a category. Will check for these first...
        # Only year (0), country(2) and city(3) are available. (1) is continent
        if matches[1] in ("Africa","Asia","Europe","NorthAmerica","SouthAmerica","Oceania","Antarctica"):
            title = f"{args.event_name} {matches[0]} {matches[3]} ({matches[2]})"
            location_id = getlocation_id(city=matches[3], country=matches[2])
        else:
            # Year (0)0, country (1), city (2) and organization (3) are all available
            title = f"{args.event_name} {matches[0]} {matches[3]} ({matches[2]}, {matches[1]})"
            location_id = getlocation_id(organization=matches[3], city=matches[2], country=matches[1])
    elif len(matches) > 2:
        # Only year (0), country(1) and city(2) are available
        title = f"{args.event_name} {matches[0]} {matches[2]} ({matches[1]})"
        location_id = getlocation_id(city=matches[2], country=matches[1])
    elif len(matches) > 1:
        # Only year and country is available
        title = f"{args.event_name} {matches[0]} {matches[1]}"
        location_id = getlocation_id(country=matches[1])
    elif len(matches) == 1:
        # Only year is available
        title = f"{args.event_name} {matches[0]}"
        location_id = 0
    else:
        # General fallback
        title = f"{args.event_name} - general"
        location_id = "-"
    year = matches[0][:4] if matches else "1900"
    nextdate = get_next(args.event_name, year)

    try:
        live_version = os.path.join(filepath, 'revisions', revision.replace('/', os.path.sep))

        if not os.path.isfile(live_version):
            live_version = os.path.join(filepath, 'revisions', prev_revision.replace('/', os.path.sep))

        if os.path.isfile(live_version):
            with open(live_version, encoding='utf-8') as markup_file:
                markup = markup_file.read()
                logging.debug(f"Original Markup: {repr(markup)}")  # Debugging output
                html = markup2html(markup).replace('"', "'").replace("\n", "")
                logging.debug(f"Converted HTML: {repr(html)}")  # Debugging output
                html_with_links = convert_wiki_links_to_html(html, file, filename)
        else:
            logging.error(f">> Failed to find live version for {filepath}.")
            return False
    except Exception as e:
        logging.error(f"Error: {e} - skipping {filepath}")
        return False

    footer = f"<br><hr>Created by {created_author_details['name']} on {last_modified_date}." if created_author_details[
                                                                                                    'name'] == \
                                                                                                modified_author_details[
                                                                                                    'name'] else f"<br><hr>Created by {created_author_details['name']} on {created_date} - Last modified by {modified_author_details['name']} on {last_modified_date}."

    row = {
        'title': title,
        'start_date': f"{nextdate} 00:00:00",
        'end_date': f"{nextdate} 23:59:59",
        'calid': calid,
        'all_day': '1',
        'location_ids': [location_id],
        'description': html_with_links + footer
    }

    return row


def cleanup_name(moinfile):
    cleaned = os.fsdecode(moinfile)
    cleaned = replace_hex_codes(cleaned)
    return cleaned


def make_request(method="GET", endpoint="/dpcalendar/events", data=None, params=None):
    url = f"{domain}/api/index.php/v1{endpoint}"

    headers = {
        'Accept': 'application/vnd.api+json',
        'Content-Type': 'application/json',
        'X-Joomla-Token': token
    }
    try:
        response = requests.request(method=method, url=url, headers=headers, json=data, params=params)
        if response.status_code == 200:
            return response.json()  # Directly return JSON response
        else:
            if data is None or 'city' not in data or data['city'] == 'Unknown':
                logging.warning(f">> WARNING {response.status_code} ({response.reason}) Skipping general page for data {data} on {url}")
            logging.error(f">> ERROR {response.status_code} ({response.reason}) {method} {endpoint}")

            title = data['title'] if data and 'title' in data else 'No title'
            logging.error(f">> ERROR {response.status_code} ({response.reason}) {method} {endpoint} {title}")
            logging.error(f"Response content: {response.content}")  # Log response content for debugging
            sleep(5)

            return None

    except requests.exceptions.RequestException as e:
        logging.error(f">> Request exception: {e}")
        return None

    # Failsafe - should never occur
    return None


def main():
    skipped_counter = 0
    copied_counter = 0

    for source_file in os.listdir(pages_path):
        filename = cleanup_name(source_file)
        logging.info("Processing:", filename)  # Print the processed filename
        source = os.path.join(pages_path, os.fsdecode(source_file))
        if filename.startswith("20") and len(filename) > 4:
            event_info = get_current(source, source_file, filename)
        else:
            event_info = False

        if event_info:
            add_event = True   # Initialize value

            candidate_events = make_request(method="GET", endpoint="/dpcalendar/events", params={
                "filter['search']": event_info['title'],
                "page[limit]": 5000
            })
            if candidate_events:
                try:
                    pages = int(candidate_events['meta']['total-pages'])
                except Exception as e:
                    pages = 1
                    print(f"{e}")

                for page in range(pages):
                    logging.info(f"Loading events page {page}/{pages}")

                    if page > 0:
                        candidate_events = make_request(method="GET", endpoint="/dpcalendar/events", params={
                            "filter['search']": event_info['title'],
                            "page[offset]": page + 1,
                            "page[limit]": 5000
                        })

                    for candidate in candidate_events['data']:
                        # Still iterating through the results just to prevent "near hits"

                        if candidate['attributes']['title'] == event_info['title']:
                            # Break out if a duplicate event is found
                            add_event = False
                            break

            if add_event:
                # Submit the event
                response = make_request(method="POST", endpoint="/dpcalendar/events", data=event_info)
                if response:
                    copied_counter += 1
                    logging.info(f"Copied event {event_info['title']}")
                else:
                    skipped_counter += 1
                    logging.warning(f">> Event failed")


        else:
            skipped_counter += 1
            logging.info(f">> SKIPPED EVENT - NO EVENT INFO")

    logging.info(f">> SUMMARY: Copied {copied_counter} pages and skipped {skipped_counter} pages.")


if __name__ == "__main__":
    main()
