# Moin2HTML

Moinmoin 1.x has existed for decades, and once was one of the very promising wiki systems.
Currently, there is a perpetual development version of Moinmoin 2.0 but there is no perspective of a stable release.

For the migration of the wiki pages of Software Freedom Day and aligned events, we need to migrate thousands of pages to Joomla.
The idea of this project is to create a migration path that allows to migrate pages as good as possible into Joomla als static content pages.

## Usage
```
python3 ./main.py -s sourcepath/ -t targetpath/ -o /images/ -d debuglevel
```
* -s = where is the /pages/ folder of the moinmoin wiki to be found?
* -t = where should the script build the pages?
* -i = where will the images be stored online?
* -d = message detail - multiple options
  * hide_skipped = don't show the list of skipped folders/files
  * show_skipped = also show the list of skipped folders/files
  * debug = show every message during the process
  * minimal = only show summary at the end

In the end, "minimal" will be the default. Currently it is set to "debug"

## Roadmap
- [x] Import the latest version of every page into the system
- [x] Respect the folder hierarchy of the pages in the wiki (convert filenames to paths) and support UTF-8
- [x] Migrate images to the same folder structure
- [x] Embed images and href links
- [x] Image links have an online base root
- [x] Implement command line parameters using argparse
- [ ] Set up requirements and deploy info
- [ ] Improve support for UTF-8 in folder and filenames
- [ ] Clean up the page templating cruft
- [ ] Manual step to clean up unwanted pages
- [ ] Retain metadata such as author name and creation date in medatada file

## Features
- [ ] Prepare the code to turn it into Joomla SQL lines
