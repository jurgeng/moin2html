#!/usr/bin/env python3

import os
import logging
from wikimarkup.parser import Parser   # pip install py-wikimarkup
import re
import argparse
from shutil import copyfile
import datetime
import json


parser = argparse.ArgumentParser(description="Parse a moin page dump")

parser.add_argument("-s", dest="src_path", default=r"C:\Users\gangs\DFD\freedom days\dfd2017\dfd2017\core\data",
                    help="Location of the path containing the data folder")
parser.add_argument("-t", dest="target_path", default=r"C:\Users\gangs\DFD\freedom days\wiki",
                    help="Path storing the output results")
parser.add_argument("-o", dest="online_path", default="/images",
                    help="Base path where images will be found online")
parser.add_argument("-d", dest="detail", default="debug",
                    choices=['warning', 'error', 'info', 'info'],
                    help="Options: warning, error, info, debug")
parser.add_argument("-n", dest="event_name", default="Document Freedom Day",
                    help="Full name of the event to parse pages for - defaults to 'Document Freedom Day'")


args = parser.parse_args()

# Define the mapping between 'detail' argument values and logging levels
detail_to_logging_level = {
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'info': logging.INFO,
    'debug': logging.DEBUG
}

# Configure logging based on the 'detail' argument
logging_level = detail_to_logging_level.get(args.detail, logging.DEBUG)
logging.basicConfig(level=logging_level, format='%(asctime)s - %(levelname)s - %(message)s')

src_path = args.src_path
pages_path = os.path.join(src_path, "pages")
users_path = os.path.join(src_path, "user")
target_path = args.target_path
online_path = args.online_path


def hex_to_utf8(match):
    hex_string = match.group(1)
    utf8_string = ''

    # Split hex string into 2-char elements and convert to UTF-8
    for i in range(0, len(hex_string), 2):
        hex_char = hex_string[i:i + 2]
        utf8_string += bytes.fromhex(hex_char).decode('utf-8', 'replace')

    return utf8_string


def replace_hex_codes(s):
    # Regular expression pattern to find hexadecimal codes within parentheses
    pattern = re.compile(r'\(([0-9a-fA-F]+)\)')

    # Substitute found hex codes with their UTF-8 characters
    result_string = pattern.sub(hex_to_utf8, s)
    return result_string


def markup2html(content):
    markupParser = Parser()
    return markupParser.parse(content, show_toc=False)


def convert_wiki_links_to_html(html_content, file, filename, base_url=""):
    # Function to remove unwanted list elements from HTML
    def remove_unwanted_lists(match):
        list_content = match.group(0)
        # Check if the list elements are at the beginning of the HTML
        if html_content.startswith(list_content):
            return ''  # Remove the entire list
        return list_content

    # Regular expression pattern to find wiki links
    wiki_link_pattern = re.compile(r'\[\[([^]|]+)(?:\|([^]]+))?]]')

    # Function to convert wiki link match objects to HTML links
    def replace_wiki_link(match):
        page_name = re.sub('<[^<]+?>', '', match.group(1))
        try:
            link_text = re.sub('<[^<]+?>', '', match.group(2)) or page_name
        except TypeError as e:
            link_text = page_name
            logging.debug(f"Type Error on {link_text} - {type(e)}")
        except Exception as e:
            logging.error(f">> FAIL: {type(e)}")
            link_text = page_name
        href = base_url + page_name.replace(' ', '_') + '/index.html'
        return f'<a href="{href}">{link_text}</a>'

    # Regular expression pattern to find wiki attachments (images usually)
    wiki_attachments_pattern = re.compile(r'\{\{attachment: ?([^}]+)\}\}')

    # Function to convert wiki attachment to HTML image links
    def replace_wiki_attachments(match, file, filename):
        image_parts = match.group(1).split("|", 2)
        image_name = image_parts[0]
        image_alt = image_parts[1] if len(
            image_parts) > 1 else image_name.split(".")[0]
        image_style = "style='" + \
            image_parts[2] + ";'" if len(image_parts) > 2 else ""

        # Attachments can have a comment field and markup too, divided by pipe symbols |
        # First block (always present) is filename, second is alt text, third and onward are CSS
        # Example: {{attachment:myLogo.png|My Organisation|width=100%,align="center"}}

        # copy image from attachments to new folder location
        from_path = os.path.join(pages_path, file if isinstance(file, str) else file.decode("utf-8"), "attachments")
        to_path = os.path.join(target_path, filename)
        href_path = online_path + filename + image_name
        if os.path.isfile(os.path.join(from_path, image_name)):
            try:
                if not os.path.exists(to_path):
                    os.makedirs(to_path)
                copyfile(os.path.join(from_path, image_name), os.path.join(to_path, image_name))
            except Exception as e:
                logging.warning(f">> SKIPPED FILE {os.path.join(from_path, image_name)} - reason: {type(e)}")

        return f'<img src="{href_path}" alt="{image_alt}" {image_style}/>'

    # Replace wiki links with HTML links
    html_with_links = wiki_link_pattern.sub(replace_wiki_link, html_content)
    # html_with_images = wiki_attachments_pattern.sub(replace_wiki_attachments, html_with_links)
    html_with_images = wiki_attachments_pattern.sub(
        lambda match: replace_wiki_attachments(match, file, filename), html_with_links
    )

    # Remove unwanted lists from the HTML
    html_without_lists = re.sub(
        r'<ol>.*?</ol>', remove_unwanted_lists, html_with_images, flags=re.DOTALL)

    return html_without_lists


def convert_epoch_to_date(epoch_millis):
    # Convert the epoch time in milliseconds to a human-readable date
    return datetime.datetime.fromtimestamp(int(epoch_millis) / 1000000.0).strftime('%Y-%m-%d %H:%M:%S')


def extract_edit_log_info(edit_log_path):
    try:
        with open(edit_log_path, 'r') as file:
            lines = file.readlines()

            if not lines:
                return None, None, None

            first_line = lines[0].split('\t')
            last_line = lines[-1].split('\t')

            # Convert timestamps to readable dates
            created_date = convert_epoch_to_date(first_line[0])
            created_user_id = first_line[6].strip()
            modified_date = convert_epoch_to_date(last_line[0])

            # Extract user ID from the last line
            modified_user_id = last_line[6].strip()

            return created_date, modified_date, created_user_id, modified_user_id

    except FileNotFoundError:
        logging.warning(f"Edit-log file not found: {edit_log_path}")
    except Exception as e:
        logging.error(f"Error reading edit-log file: {e}")

    return None, None, None


def get_author_details(user_id, user_data_directory):
    user_file_path = os.path.join(user_data_directory, user_id)
    author_details = {'name': 'Unknown', 'email': 'Unknown'}

    try:
        with open(user_file_path, 'r') as user_file:
            # This part depends on how the user data is formatted
            # Let's assume it's a simple text file with 'Name: XXX' and 'Email: XXX'
            for line in user_file:
                if line.startswith('name='):
                    author_details['name'] = line.split('=', 1)[1].strip()
                elif line.startswith('email='):
                    author_details['email'] = line.split('=', 1)[1].strip()

    except FileNotFoundError:
        logging.warning(f"User file not found for user ID {user_id}")
    except Exception as e:
        logging.error(f"Error while reading user file for user ID {user_id}: {e}")

    return author_details


def get_current(filepath, file, filename):

    metadata = {}   # Start off with empty metadata (will gradually populate)
    current_path = os.path.join(filepath, "current")
    if not os.path.isfile(current_path):
        logging.warning(f">> SKIPPED {filepath}. Reason: No 'current' file.")
        return False, metadata

    try:
        with open(current_path) as current:
            raw_content = current.read()
            logging.debug(f"Raw Content: {repr(raw_content)}")
            # Check if unwanted content is at the beginning of the HTML
            if raw_content.startswith('<ol><li>'):
                raw_content = re.sub(
                    r'<ol>.*?</ol>', '', raw_content, flags=re.DOTALL)
            revision = raw_content.strip()
    except FileNotFoundError as e:
        logging.warning(f">> File not found: current version for {current_path}. Reason: {e}")
        return False, metadata
    except IOError as e:
        logging.warning(f"Error reading file {current_path}: {e}")
        return False, metadata
    except Exception as e:
        logging.warning(f">> General error - Reason: {e}")
        return False, metadata

    edit_log_path = os.path.join(filepath, "edit-log")
    created_date, last_modified_date, created_user_id, modified_user_id = extract_edit_log_info(edit_log_path)
    created_author_details = get_author_details(created_user_id, users_path)
    modified_author_details = get_author_details(modified_user_id,users_path)

    matches = re.findall(r"(?:^|/)([^/]+)", filename)
    if len(matches) > 3:
        # Year, country, city and organization are all available
        title = f"{args.event_name} {matches[0]} {matches[3]} ({matches[2]}, {matches[1]})"
    elif len(matches) > 2:
        # Only year, country and city are available
        title = f"{args.event_name} {matches[0]} {matches[2]} ({matches[1]})"
    elif len(matches) >1:
        # Only year and country is available
        title = f"{args.event_name} {matches[0]} {matches[1]}"
    elif len(matches) == 1:
        # Only year is available
        title = f"{args.event_name} {matches[0]}"
    else:
        # General fallback
        title = f"{args.event_name} - general"


    metadata.update({
        # ... other metadata ...
        'created': created_date,
        'modified': last_modified_date,
        'created_by_alias': created_author_details['name'],
        'note': f"{created_author_details['email']} - modified by {modified_author_details['name']}({modified_author_details['email']})",
        'title': title
    })

    live_version = os.path.join(filepath, 'revisions', revision.replace('/', os.path.sep))
    if os.path.isfile(live_version):
        with open(live_version, encoding='utf-8') as markup_file:
            markup = markup_file.read()

            # Extract additional metadata from markup content
            # Extract language
        language_match = re.search(r'^#language (\w\w)$', markup, re.MULTILINE)
        if language_match:
            metadata['lang_string'] = language_match.group(1)

        # Extract title
        title_match = re.search(r'^#title (.+)$', markup, re.MULTILINE)
        if title_match:
            metadata['title'] = title_match.group(1)

        # Extract tags -> skipping for now, doesn't work
        # tags_match = re.search(r'^#tags (.+)$', markup, re.MULTILINE)
        # if tags_match:
        #     metadata['tags'] = tags_match.group(1).split(', ')  # Assuming tags are separated by comma and space
        #
        #     html = markup2html(markup)
        #     html_with_links = convert_wiki_links_to_html(html, file, filename)
        #
        #     return html_with_links, metadata

    # TODO: Extract metadata from original file here.
    # {'author': 'AUTHORNAME',
    #  OK -> 'title' : 'EventName - YEAR - Country - Lowest level'
    #  'year' : '20XX',
    #  OK -> 'created_date' : 'YYYY-MM-DD-HH-SS+00",
    #  OK -> 'last_modified': 'YYYY-MM-DD-HH-SS+00",
    #  OK -> 'language': 'LL'
    #  OK -> 'tags' : ['CategoryXXX', 'othertags', ...]
    # }
    #       - check if file starts with "#language LL" (LL = language) -> add language to metadata
    #       - extract year from current_path
    #       - add CategoryXXX from footer to tag list in metadata

    # TODO: extract author from file -> more complicated, will require separate function
    # open file edit-log in root of page = tab separated file
    # INFO: https://moinmo.in/MoinDev/Storage
    # -> column 1 = epoch in milliseconds
    # column 2 = revision number
    # column 3 = Edit type (should be SAVENEW or SAVE)
    # column 4 = Filename
    # column 5 = IP address
    # column 6 = hostname
    # -> column 7 = user ID  -> needs to be looked up
    # CREATED DATE = column 1 from first row
    # LAST MODIFIED DATE = column 1 from last row
    # /core/data/user/USERID shows data about user
    # extract fields name and email

    # TODO: if all metadata is extracted: remove footer and possible to define other cruft

    live_version = os.path.join(filepath, 'revisions', revision.replace('/', os.path.sep))
    if os.path.isfile(live_version):
        with open(live_version, encoding='utf-8') as markup_file:
            markup = markup_file.read()
            logging.debug(f"Original Markup: {repr(markup)}")  # Debugging output
            html = markup2html(markup)
            logging.debug(f"Converted HTML: {repr(html)}")  # Debugging output
            html_with_links = convert_wiki_links_to_html(html, file, filename)

            return html_with_links, metadata
    else:
        logging.error(f">> Failed to find live version for {filepath}.")
        return False, metadata


def cleanup_name(moinfile):
    cleaned = os.fsdecode(moinfile)
    cleaned = replace_hex_codes(cleaned)
    return cleaned


def main():
    skipped_counter = 0
    copied_counter = 0

    for file in os.listdir(pages_path):
        filename = cleanup_name(file)
        logging.info("Processing:", filename)  # Print the processed filename
        source = os.path.join(pages_path, os.fsdecode(file))
        html_content, metadata_content = get_current(source, file, filename)
        if html_content:
            try:
                os.makedirs(os.path.join(target_path, filename), exist_ok=True)
            except FileExistsError:
                logging.warning(f"file {filename} already exists. Skipping it")
                pass
            except OSError as e:
                logging.warning(f"Error creating directory {filename}: {e}")
                continue

            target = os.path.join(target_path, filename, "index.html")
            metadata_target = os.path.join(target_path, filename, "metadata.json")
            try:
                with open(target, 'w', encoding='utf-8') as target_file:
                    target_file.write(html_content)
                with open(metadata_target, 'w', encoding='utf-8') as metadata_file:
                    # Convert the metadata_content to a JSON string
                    json_data = json.dumps(metadata_content, ensure_ascii=False, indent=4)
                    # Write the JSON string to the file
                    metadata_file.write(json_data)
            except IOError as e:
                logging.error(f"Error writing to file: {e}")
                continue

            copied_counter += 1
            logging.info(f">> COPIED {source} as HTML in {target} and metadata in {metadata_target}")
        else:
            skipped_counter += 1
            logging.info(f">> SKIPPED {source}")

    logging.info(f">> SUMMARY: Copied {copied_counter} pages and skipped {skipped_counter} pages.")


if __name__ == "__main__":
    main()
